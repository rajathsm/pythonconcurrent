from itertools import permutations
import concurrent.futures
import time

start = time.perf_counter()

d=[''.join(p) for p in permutations('123956789')]# if int(''.join(p))>int(2017)]

#print("length",len(d))
f=[]
for  i in d:

	f.append(int(i))

h=list(set(f))
h.sort()


tar=123456789
glo=[]

lst1=[t+10 for t in range(len(h)) if t%10==0]
p=0
lst2=lst1.copy()
lst2=[p,*lst2]
d=[]
for c,v in zip(lst2,lst1):
	d.append((c,v))

def do_something(fg):
	ab=[]
	x,y=fg
	nLst=h[x:y]
	for i in nLst:
		ab.append(abs(i-tar))

	
	if min(ab)==0:
		ab.remove(0)
	
	glo.append(min(ab))
    
    


with concurrent.futures.ThreadPoolExecutor() as executor:
    
    results = executor.map(do_something,d)

print(min(glo)+int(tar))

finish = time.perf_counter()

print(f'Finished in {round(finish-start, 2)} second(s)')